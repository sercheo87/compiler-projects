projectManager {
    server = 'localhost'
    port = 25
    mapConfigurationsProjects = []
    fileSvn = './coments-svn.txt'
    fileYml = './projects-alias.yml'
}

environments {
    dev {
        serverName = 'http://localhost:9090'
        fileSvn = './coments-svn.txt'
    }

    test {
        serverName = 'http://testserver'
        mail {
            server = 'mail.testserver'
        }
    }

    prod {
        serverName = 'http://www.mrhaki.com'
        mail {
            port = 552
            server = 'mail.host.com'
        }
    }
}