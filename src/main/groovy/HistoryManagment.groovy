/**
 * Created by sergio on 2/9/16.
 */
import groovy.json.JsonSlurper

def collectionFiles = new JsonSlurper().parseText(args[0])
def version = args[1]
def comment = args[2]
def project = args[3]

collectionFiles[0].each { k, v ->
    println "${v}"
    println "Editing readme [$v]"
    File file = new File(v)
    file.append("\n${project}")
    file.append("\n\t$comment - [${version} -> ${version}]")
}