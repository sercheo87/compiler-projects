/**
 * Created by sergio on 2/9/16.
 */

println "Arguments ${args}"

def folderToCompile = args[0].toString()

println "Folder to Compile ${folderToCompile}"

def process = "mvn clean".execute(null, new File(folderToCompile))
println "${process.text}"
println process.exitValue()

